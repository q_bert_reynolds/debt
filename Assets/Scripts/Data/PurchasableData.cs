﻿using UnityEngine;
using System.Collections;

public class PurchasableData : ScriptableObject {

	[Multiline]
	public string description = "Buy me.";
	public float cost = 10;

	public virtual string GetLongDescription () {
		return description;
	}
}
