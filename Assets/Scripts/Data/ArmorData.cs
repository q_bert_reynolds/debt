﻿using UnityEngine;
using System.Collections;

public class ArmorData : PurchasableData {

	public float defense = 0f;
	public float weight = 0f;
}
