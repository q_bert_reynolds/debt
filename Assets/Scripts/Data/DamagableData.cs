﻿using UnityEngine;
using System.Collections;

public class DamagableData : ScriptableObject {

	public delegate void OnHealthChange(float health);
	public event OnHealthChange onHealthChange = delegate {};
	
	[Multiline]
	public string description = "A weak door.";

	[SerializeField] 
	[HideInInspector]
	private float _health = 100;
	public float health {
		get {
			return _health;
		}
		set {
			if (value != _health) {
				_health = Mathf.Max(value, 0);
				onHealthChange(_health);
			}
		}
	}
}
