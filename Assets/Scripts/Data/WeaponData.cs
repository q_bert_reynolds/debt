﻿using UnityEngine;
using System.Collections;

public class WeaponData : DamagerData {

	public ProjectileData projectile;

	public float range = 1f;
	public float duration = 0.1f;

	[Range(0,1)]
	public float durability = 1;

	[Range(1,360)]
	public float angle = 1f;

	public override string GetLongDescription () {
		string desc = description + "\n";
		foreach (EffectData effectData in effectsData) {
			desc += effectData.description + "\n";
		}
		return desc;
	}
}
