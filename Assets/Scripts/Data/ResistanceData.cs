﻿using UnityEngine;
using System.Collections;

public class ResistanceData : ScriptableObject {

	[Multiline]
	public string description = "Some itching in the groin area.";
	
	public EffectType effectType = EffectType.None;

	[Range(0,1)]
	public float chanceReduction = 0;

	[Range(0,1)]
	public float durationReduction = 0;
	
	[Range(0,1)]
	public float damagePerSecondReduction = 0;
	
	[Range(0,1)]
	public float knockBackReduction = 0;

	[Range(0,1)] 
	public float moveSpeedChangeReduction = 0;
}
