﻿using UnityEngine;
using System.Collections;

public enum EffectType {
	None,
	Fire,
	Ice,
	Poison,
	Wind,
	Psychic,
	Electric
	// more pokemon things here
}

public class EffectData : ScriptableObject {

	[Multiline]
	public string description = "Some itching in the groin area.";
	
	public EffectType effectType = EffectType.None;

	[Range(0,1)]
	public float chance = 0;

	public float duration = 0;
	public float damagePerSecond = 0;
	public float knockBack = 0;

	[Range(0,1)] 
	public float moveSpeedChange = 0; // 1 = can't move, 0 = no change
}
