﻿using UnityEngine;
using System.Collections;

public class CreditCardData : ScriptableObject {

	[Multiline]
	public string description = "Your uncle's card.";

	public float balance = 0f;
	public float limit = 100f;
	public float rate = 0.25f;
	public float absMinPay = 15f;
	public float minPayPct = 0.01f;

	public float minPaymentDue {
		get {
			if (balance <= absMinPay) {
				return balance;
			}
			else {
				return Mathf.Max(absMinPay, balance * minPayPct);
			}
		}
	}

	private int _minPayMissCount = 0;
	public int minPayMissCount {
		get { return _minPayMissCount; }
	}

	public void UpdateDebt () {
		balance += balance * rate;
	}

	public bool CanBuy (PurchasableData purchasable) {
		return ((limit - balance) >= purchasable.cost);
	}

	public void Pay (float payment) {
		if (payment < minPaymentDue) {
			_minPayMissCount++;
		}
		balance -= payment;
	}
}
