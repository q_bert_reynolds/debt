﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(WeaponData))]
public class WeaponDataEditor : Editor {

	[MenuItem("Assets/Create/Weapon")]
	public static void Create () {
		ScriptableObjectUtility.CreateAsset<WeaponData>();
	}
}
