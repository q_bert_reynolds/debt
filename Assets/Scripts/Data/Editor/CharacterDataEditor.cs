﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(CharacterData))]
public class CharacterDataEditor : Editor {

	[MenuItem("Assets/Create/Character")]
	public static void Create () {
		ScriptableObjectUtility.CreateAsset<CharacterData>();
	}
}
