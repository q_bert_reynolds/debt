﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(ArmorData))]
public class ArmorDataEditor : Editor {

	[MenuItem("Assets/Create/Armor")]
	public static void Create () {
		ScriptableObjectUtility.CreateAsset<ArmorData>();
	}
}
