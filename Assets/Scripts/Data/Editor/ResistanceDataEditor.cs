﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(ResistanceData))]
public class ResistanceDataEditor : Editor {

	[MenuItem("Assets/Create/Resistance")]
	public static void Create () {
		ScriptableObjectUtility.CreateAsset<ResistanceData>();
	}
}
