﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(ProjectileData))]
public class ProjectileDataEditor : Editor {

	[MenuItem("Assets/Create/Projectile")]
	public static void Create () {
		ScriptableObjectUtility.CreateAsset<ProjectileData>();
	}
}
