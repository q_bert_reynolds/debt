﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(CreditCardData))]
public class CreditCardDataEditor : Editor {

	[MenuItem("Assets/Create/Credit Card")]
	public static void Create () {
		ScriptableObjectUtility.CreateAsset<CreditCardData>();
	}
}
