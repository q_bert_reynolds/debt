﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(DamagableData))]
public class DamagableDataEditor : Editor {

	[MenuItem("Assets/Create/Damagable")]
	public static void Create () {
		ScriptableObjectUtility.CreateAsset<DamagableData>();
	}

	public override void OnInspectorGUI () {
		base.OnInspectorGUI();

		DamagableData data = target as DamagableData;
		float health = EditorGUILayout.FloatField("Health", data.health);
		if (health != data.health) {
			Undo.RegisterUndo(data, "update health");
			data.health = health;
		}
	}
}
