﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(EffectData))]
public class EffectDataEditor : Editor {

	[MenuItem("Assets/Create/Effect")]
	public static void Create () {
		ScriptableObjectUtility.CreateAsset<EffectData>();
	}
}
