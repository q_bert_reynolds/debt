﻿using UnityEngine;
using System.Collections;

public class ProjectileData : DamagerData {

	public enum Condition {
		LowHealth,
		FullHealth,
		Always
	}
	public Condition condition = Condition.FullHealth;
	public GameObject prefab;
	
	public float range = 1f;
	public float speed = 0.1f;
}
