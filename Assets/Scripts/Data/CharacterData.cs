﻿using UnityEngine;
using System.Collections;

public class CharacterData : ScriptableObject {

	[Multiline]
	public string description = "A young child.";

	public float attackInterval = 2; // for enemies, should be in own class?
	public float speed = 1;
	public float acceleration = 1;

	public WeaponData weaponData;
	public ArmorData armorData;
}
