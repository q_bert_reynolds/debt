﻿using UnityEngine;
using System.Collections;

public class DamagerData : PurchasableData {
	
	public EffectData[] effectsData;
	public float damage = 1f;
	
}
