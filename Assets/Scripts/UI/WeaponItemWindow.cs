﻿using UnityEngine;
using System.Collections;

public class WeaponItemWindow : ItemWindow {

	public TextMesh damageText;
	public TextMesh damageDiffText;
	public TextMesh rangeText;
	public TextMesh rangeDiffText;
	public TextMesh angleText;
	public TextMesh angleDiffText;
	public TextMesh durationText;
	public TextMesh durationDiffText;
	
	public override void UpdateUI () {
		base.UpdateUI();
		WeaponData weapon = purchasables[index] as WeaponData;
		
		damageText.text = weapon.damage.ToString();
		rangeText.text = weapon.range.ToString();
		angleText.text = weapon.angle.ToString();
		durationText.text = weapon.duration.ToString();

		WeaponData currentWeapon = GameManager.instance.character.weaponData;
		if (weapon == currentWeapon) {
			title.text = weapon.name + "(Current)";
		}

		SetDiff(currentWeapon.damage, weapon.damage, false, damageDiffText);
		SetDiff(currentWeapon.range, weapon.range, false, rangeDiffText);
		SetDiff(currentWeapon.angle, weapon.angle, false, angleDiffText);
		SetDiff(currentWeapon.duration, weapon.duration, true, durationDiffText);
	}

	public override void BuyClicked () {
		base.BuyClicked();
		WeaponData weaponData = purchasables[index] as WeaponData;
		GameManager.instance.character.weaponData = weaponData;
	}

	// public virtual bool CanBuy () {
	// 	CreditCardData creditCardData = GameManager.instance.creditCardData;
	// 	return creditCardData.CanBuy(purchasable);
	// }

	void SetDiff (float current, float other, bool reversed, TextMesh textMesh) {
		float diff = other - current;
		if (diff > 0) {
			textMesh.text = "+" + diff.ToString();
			textMesh.color = reversed?negativeColor:positiveColor;
		}
		else if (diff == 0) {
			textMesh.text = diff.ToString();
			textMesh.color = neutralColor;
		}
		else if (diff < 0) {
			textMesh.text = diff.ToString();
			textMesh.color = reversed?positiveColor:negativeColor;
		}
	}
}
