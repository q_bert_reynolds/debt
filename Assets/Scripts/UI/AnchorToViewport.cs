using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class AnchorToViewport : MonoBehaviour {
	
	public Camera cam;
	public Vector3 anchorPoint = Vector3.zero;
	
	void Start () {
		if (cam == null) cam = Camera.mainCamera;
		anchorPoint.z = transform.localPosition.z;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = cam.ViewportToWorldPoint(anchorPoint);
	}
}
