﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {

	public delegate void OnClick();
	public event OnClick onClick = delegate {};

	void OnMouseDown () {
		onClick();
	}
}
