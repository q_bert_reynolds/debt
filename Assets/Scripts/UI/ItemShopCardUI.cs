﻿using UnityEngine;
using System.Collections;

public class ItemShopCardUI : MonoBehaviour {

	public TextMesh nameTextMesh;
	public TextMesh balanceTextMesh;
	public TextMesh limitTextMesh;

	public void Start () {
		UpdateCard();
	}

	public void UpdateCard () {
		CreditCardData creditCardData = GameManager.instance.creditCardData;
		nameTextMesh.text = creditCardData.name;
		balanceTextMesh.text = "$" + creditCardData.balance.ToString();
		limitTextMesh.text = "$" + creditCardData.limit.ToString();
	}
}
