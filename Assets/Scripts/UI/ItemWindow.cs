﻿using UnityEngine;
using System.Collections;

public class ItemWindow : MonoBehaviour {

	public ItemShopCardUI itemShopCardUI;

	public TextMesh title;
	public TextMesh description;
	public TextMesh cost;

	public Button buyButton;
	public Button leftButton;
	public Button rightButton;
	public PurchasableData[] purchasables;

	public Color positiveColor = Color.green;
	public Color negativeColor = Color.red;
	public Color neutralColor = Color.black;
	protected int index = 0;

	void OnEnable () {
		buyButton.onClick += BuyClicked;
		leftButton.onClick += LeftClicked;
		rightButton.onClick += RightClicked;
	}

	void OnDisable () {
		buyButton.onClick -= BuyClicked;
		leftButton.onClick -= LeftClicked;
		rightButton.onClick -= RightClicked;
	}

	void Awake () {
		if (itemShopCardUI == null) {
			itemShopCardUI = GameObject.FindObjectOfType(typeof(ItemShopCardUI)) as ItemShopCardUI;
		}
	}

	public virtual void BuyClicked () {
		PurchasableData purchasable = purchasables[index];
		CreditCardData creditCardData = GameManager.instance.creditCardData;
		creditCardData.balance += purchasable.cost;
		itemShopCardUI.UpdateCard();
		title.text = purchasable.name + "(Current)";
		buyButton.gameObject.SetActive(false);
	}

	void LeftClicked () {
		index = (index + purchasables.Length - 1) % purchasables.Length;
		UpdateUI();
	}

	void RightClicked () {
		index = (index + 1) % purchasables.Length;
		UpdateUI();
	}

	public virtual bool CanBuy (PurchasableData purchasable) {
		CreditCardData creditCardData = GameManager.instance.creditCardData;
		return creditCardData.CanBuy(purchasable);
	}

	public virtual void UpdateUI () {
		PurchasableData purchasable = purchasables[index];
		title.text = purchasable.name;
		description.text = purchasable.GetLongDescription();
		cost.text = "$" + purchasable.cost.ToString();
		buyButton.gameObject.SetActive(CanBuy(purchasable));
	}
}
