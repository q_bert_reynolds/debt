﻿using UnityEngine;
using System.Collections;

public class GameTimer : MonoBehaviour {

	public TextMesh textMesh;

	float startTime;

	void Start () {
		startTime = Time.time;
	}
	
	void Update () {
		float timeSoFar = Time.time - startTime;
		int count = (int)Mathf.Max(GameManager.instance.gameLength - timeSoFar, 0);
		textMesh.text = count.ToString();
		if (count == 0) GameManager.instance.EndGame();
	}
}
