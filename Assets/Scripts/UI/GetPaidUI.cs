﻿using UnityEngine;
using System.Collections;

public class GetPaidUI : MonoBehaviour {

	public CreditCardData[] cards;
	public TextMesh textMesh;

	public string princessText;
	public string over50;
	public string under50;
	public string under10;

	public void Start () {
		string txt = princessText;
		float pct = GameManager.instance.GetPctKilled();
		
		if (pct > 0.5f) {
			txt += over50;
			txt += "\n You've unlocked the " + cards[GameManager.instance.level];
		}
		else if (pct < 0.1f) {
			txt += under10;
		}
		else {
			txt += under50;
		}

		txt = txt.Replace("(NUMBER)", GameManager.instance.GetDeathToll().ToString());
		txt = txt.Replace("(AMOUNT)", ((int)(pct * 100)).ToString() + "%");

		textMesh.text = txt;

		CreditCardData credit = GameManager.instance.creditCardData;
		credit.Pay(credit.balance * pct);

		if (pct > 0.5f) {
			CreditCardData lastCard = GameManager.instance.creditCardData;
			GameManager.instance.creditCardData = cards[GameManager.instance.level];
			GameManager.instance.creditCardData.balance = lastCard.balance;
			GameManager.instance.level++;
		}
	}
}
