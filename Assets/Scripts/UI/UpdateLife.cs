﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(TextMesh))]
public class UpdateLife : MonoBehaviour {

	public DamagableData damagableData;

	private TextMesh textMesh;

	void Awake () {
		textMesh = GetComponent<TextMesh>();
		OnHealthChange(damagableData.health);
	}

	void OnEnable () {
		damagableData.onHealthChange += OnHealthChange;
	}

	void OnDisable () {
		damagableData.onHealthChange -= OnHealthChange;
	}

	void OnHealthChange (float health) {
		textMesh.text = "Life: " + Mathf.CeilToInt(health);
	}
}
