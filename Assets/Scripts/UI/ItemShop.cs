﻿using UnityEngine;
using System.Collections;

public class ItemShop : MonoBehaviour {

	public TextMesh titleText;
	public ItemWindow[] itemWindows;
	public Button leftButton;
	public Button rightButton;

	private int index = 0;

	void OnEnable () {
		leftButton.onClick += LeftClicked;
		rightButton.onClick += RightClicked;
	}

	void OnDisable () {
		leftButton.onClick -= LeftClicked;
		rightButton.onClick -= RightClicked;
	}

	void Start () {
		UpdateUI();
	}

	void LeftClicked () {
		index = (index + itemWindows.Length - 1) % itemWindows.Length;
		UpdateUI();
	}

	void RightClicked () {
		index = (index + 1) % itemWindows.Length;
		UpdateUI();
	}

	void UpdateUI () {
		for (int i = 0; i < itemWindows.Length; i++) {
			itemWindows[i].gameObject.SetActive(i == index);
		}
		ItemWindow itemWindow = itemWindows[index];
		titleText.text = itemWindow.name;
		itemWindow.UpdateUI();
	}
}
