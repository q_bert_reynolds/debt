﻿using UnityEngine;
using System.Collections;

public class Enemy : Character {

	public delegate void OnEnemyKilled();
	public static event OnEnemyKilled onEnemyKilled = delegate {};
	
	public float stopDist = 0.1f;
	public float targetRadius = 10;
	public float retargetInterval = 1;
	public LayerMask targetLayer;
	
	private Transform target;
	private float timeOfLastAttack = 0;

	protected override void Awake () {
		base.Awake();
		damagableData = GameObject.Instantiate(damagableData) as DamagableData;
	}

	void OnEnable () {
		StartCoroutine("GetTarget");
	}

	void OnDisable () {
		StopCoroutine("GetTarget");
	}

	void Update () {
		if (target == null) {
			// wander
		}
		else {
			Vector3 diff = target.position - transform.position;
			float targetExtents = 0;
			if (target.collider != null) targetExtents = target.collider.bounds.extents.x;		
			if (diff.magnitude > collider.bounds.extents.x + targetExtents + stopDist) {
				motor.targetVelocity = diff.normalized * characterData.speed;
			}
			else {
				motor.targetVelocity = Vector3.zero;
			}

			bool canAttack = (Time.time - timeOfLastAttack > characterData.attackInterval);
			bool shouldAttack = (diff.magnitude <= characterData.weaponData.range + collider.bounds.extents.x);
			if (canAttack && shouldAttack) {
				float angle = Mathf.Atan2(-diff.z, diff.x) * Mathf.Rad2Deg;
				StartCoroutine("Attack", angle);
				timeOfLastAttack = Time.time;
			}
		}
	}

	IEnumerator GetTarget () {
		while (enabled) {
			Collider[] colliders = Physics.OverlapSphere(transform.position, targetRadius, targetLayer);
			if (colliders.Length > 0) {
				target = colliders[0].transform;
			}
			else {
				target = null;
			}
			yield return new WaitForSeconds(retargetInterval);
		}
	}

	public override void Die () {
		base.Die();
		onEnemyKilled();
	}
}
