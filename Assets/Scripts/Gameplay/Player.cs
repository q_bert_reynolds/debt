﻿using UnityEngine;
using System.Collections;

public class Player : Character {

	void Start () {
		GameManager.instance.StartGame();
	}

	void Update () {
		float x = Input.GetAxis("Horizontal");
		float z = Input.GetAxis("Vertical");

		motor.targetVelocity = new Vector3(x, 0, z) * characterData.speed;
		//motor.shouldJump = Input.GetButton("Jump");

		bool attackPressed = Input.GetButtonDown("Fire1");
		if (attackPressed) {
			Vector3 screenPos = Camera.mainCamera.WorldToScreenPoint(weaponRotatePoint.position);
			Vector3 diff = Input.mousePosition - screenPos;
			float angle = Mathf.Atan2(-diff.y, diff.x) * Mathf.Rad2Deg;
			StartCoroutine("Attack", angle);
		}
	}

	public override void Die () {
		base.Die();
		GameManager.instance.GameOver();
	}
}
