using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CameraController : MonoBehaviour {
	
	public Transform target;
	public float lockPos = 10;
	public float minDist = 10;
	public float maxDist = 15;
	public Vector3 camPosOffset = Vector3.zero;
	public Vector3 distCheckOffset = Vector3.zero;
	public float speed = 1;
	
	private bool shouldMove = true;
	
	void Start () {
		if (target == null) target = GameObject.FindGameObjectWithTag("Player").transform;
		transform.position = target.position + camPosOffset;
	}
	
	void Update () {
		if (target == null) return;
		Vector3 destPos = target.position + camPosOffset;
		
		if (!Application.isPlaying) {
			transform.position = destPos;
			return;
		}
			
		float dist = Vector3.Distance(transform.position - camPosOffset + distCheckOffset, target.position);
		if (dist > maxDist) shouldMove = true;
		else if (dist < minDist) shouldMove = false;
		if (shouldMove) {
			transform.position = Vector3.Lerp(transform.position, destPos, speed * Time.deltaTime);
		}
	}
}
