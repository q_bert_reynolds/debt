﻿using UnityEngine;
using System.Collections;

public class Damagable : MonoBehaviour {

	public DamagableData damagableData;

	protected virtual void TakeDamage (DamagerData damager, Vector3 direction) {
		damagableData.health -= damager.damage;
		if (damagableData.health <= 0) GameObject.Destroy(gameObject);
	}

	void OnTriggerEnter (Collider collider) {
		Damager damager = collider.GetComponent<Damager>();
		if (damager != null && collider.gameObject.layer != gameObject.layer) {
			TakeDamage(damager.damagerData, transform.position - collider.transform.position);
		}
	}
}
