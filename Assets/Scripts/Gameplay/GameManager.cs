﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public float gameLength = 180;
	public int level = 0;

	public static GameManager instance;
	public CharacterData character;
	public DamagableData damagable;
	public CreditCardData creditCardData;

	private int deathToll = 0;
	private int enemiesLeft = 0;

	void Awake () {
		if (instance != null) {
			Debug.Log("too many game managers, deleting");
			GameObject.Destroy(gameObject);
			return;
		}
		instance = this;
		DontDestroyOnLoad(gameObject);
	}

	void OnEnable () {
		Enemy.onEnemyKilled += IncrementDeathToll;
	}

	void OnDisable () {
		Enemy.onEnemyKilled -= IncrementDeathToll;
	}

	public void IncrementDeathToll () {
		deathToll++;
	}

	public void StartGame () {
		deathToll = 0;
	}

	public void EndGame () {
		Enemy[] enemies = GameObject.FindObjectsOfType(typeof(Enemy)) as Enemy[];
		enemiesLeft = enemies.Length;
		Application.LoadLevel("GetPaid");
	}

	public void GameOver () {
		Application.LoadLevel("YouSuck");
	}

	public int GetDeathToll () {
		return deathToll;
	}

	public float GetPctKilled () {
		return (float)deathToll / ((float)deathToll + (float)enemiesLeft);
	}
}
