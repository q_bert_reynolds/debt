﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterMotor))]
public class Character : Damagable {

	public CharacterData characterData;
	protected CharacterMotor motor;

	public Transform weaponScalePoint;
	public Transform weaponRotatePoint;
	public Damager weapon;

	protected virtual void Awake () {
		motor = GetComponent<CharacterMotor>();
		motor.maxSpeedChange = characterData.acceleration;
		motor.maxSpeed = characterData.speed;

		WeaponData weaponData = characterData.weaponData;
		weapon.damagerData = weaponData;
		Vector3 scale = weaponScalePoint.localScale;
		scale.x = weaponData.range;

		weapon.gameObject.SetActive(false);
	}

	protected IEnumerator Attack (float startAngle) {
		if (!weapon.gameObject.activeSelf) {
			weapon.gameObject.SetActive(true);
			
			float time = 0;
			while (time < characterData.weaponData.duration) {
				float progress = time / characterData.weaponData.duration;
				float angle = characterData.weaponData.angle * (progress - 0.5f) + startAngle;
				weaponRotatePoint.rotation = Quaternion.AngleAxis(angle, Vector3.up);
				time += Time.deltaTime;
				yield return null;
			}
			weapon.gameObject.SetActive(false);
		}
	}

	protected override void TakeDamage (DamagerData damager, Vector3 direction) {
		Debug.Log(name);
		Debug.Log("Start Health: " + damagableData.health);
		float damage = Mathf.Max(damager.damage - characterData.armorData.defense, 0);
		Debug.Log("Damage: " + damage);
		damagableData.health -= damage;
		Debug.Log("Finish Health: " + damagableData.health);

		foreach (EffectData effect in damager.effectsData) {
			rigidbody.AddForce(direction * effect.knockBack);
		}

		if (damagableData.health <= 0) Die();
	}

	public virtual void Die () {
		GameObject.Destroy(gameObject);
	}
}
