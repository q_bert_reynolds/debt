using UnityEngine;
using System.Collections;
 
[RequireComponent (typeof (Rigidbody))]
[RequireComponent (typeof (CapsuleCollider))]
public class CharacterMotor : MonoBehaviour {
 
 	public float maxSpeed = 1.0f;
	public float maxSpeedChange = 1.0f;
	public float jumpHeight = 1.0f;
	public float airMovement = 0.1f;
	public float maxNorm = 30;
	public float jumpCancelRate = 0.5f;
	public LayerMask envLayer;
	
	[HideInInspector]
	public bool shouldJump = false;
	[HideInInspector]
	public bool cancelJump = false;
	public Vector3 targetVelocity = Vector3.zero;
	
	bool jumpCanceled = false;
	bool grounded = false;
 	Vector3 contactNorm = Vector3.zero;
	
	public bool isGrounded {
		get { return grounded; }
	}
	
	void Awake () {
	    rigidbody.freezeRotation = true;
	}

	public Vector3 ContactNorm () {
		return contactNorm;	
	}
	
	void FixedUpdate () {
	    Vector3 velocity = rigidbody.velocity;
        Vector3 velocityChange = (targetVelocity - velocity);
		velocityChange = velocityChange.normalized * Mathf.Clamp(velocityChange.magnitude, -maxSpeedChange, maxSpeedChange);
        velocityChange.y = 0;
 
		if (!grounded) {
			velocityChange = velocityChange * airMovement;
			if (cancelJump && !jumpCanceled && rigidbody.velocity.y > 0) {
				jumpCanceled = true;
				velocityChange.y = -rigidbody.velocity.y * jumpCancelRate;
			}
		}
		else if (shouldJump) {
			 rigidbody.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
		}
        
        rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);
        // ClampSpeed();
	    grounded = false;
	}
	
	void OnCollisionStay (Collision collision) {
		if ((envLayer & collision.gameObject.layer) == collision.gameObject.layer) {
			foreach (ContactPoint cp in collision.contacts) {
				contactNorm = cp.normal;
				float norm = Mathf.Rad2Deg * Mathf.Atan2(contactNorm.y, Mathf.Abs(contactNorm.x));
				if (norm > maxNorm) {
					grounded = true;
				}
				jumpCanceled = false;
			}
		}
	}

	void ClampSpeed () {
		Vector3 velocity = rigidbody.velocity;
        float yVel = velocity.y;
        velocity.y = 0;
        if (velocity.magnitude > maxSpeed) {
        	velocity = velocity.normalized * maxSpeed;
        }
        velocity.y = yVel;
        rigidbody.velocity = velocity;
	}
 
	float CalculateJumpVerticalSpeed () {
	    return Mathf.Sqrt(2 * jumpHeight * -Physics.gravity.y);
	}
}