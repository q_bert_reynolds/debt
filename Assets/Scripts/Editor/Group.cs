using UnityEngine;
using UnityEditor;
using System.Collections;

public class Group : ScriptableObject {
	
	[MenuItem("Tools/Add Child &%c")]
	static void AddChildToObjects() {
		Undo.RegisterSceneUndo("Add Child");
		foreach (Transform t in Selection.transforms) {
			GameObject go = new GameObject(t.name + "_child");
			go.transform.parent = t;
			go.transform.localPosition = Vector3.zero;
			go.transform.localRotation = Quaternion.identity;
			go.transform.localScale = Vector3.one;
		}
	}

	[MenuItem("Tools/Group at Origin %g")]
	static void GroupObjectsZero() {
		Undo.RegisterSceneUndo("Group");
		GameObject grp = new GameObject("group");
		grp.transform.parent = Selection.transforms[0].parent;
		grp.transform.localPosition = Vector3.zero;
		grp.transform.localRotation = Quaternion.identity;
		grp.transform.localScale = Vector3.one;
		foreach (Transform t in Selection.transforms) {
			t.parent = grp.transform;
		}
	}
	
	[MenuItem("Tools/Group at Center #%g")]
	static void GroupObjectsCenter() {
		Undo.RegisterSceneUndo("Group");
		GameObject grp = new GameObject("group");
		grp.transform.parent = Selection.transforms[0].parent;
		
		grp.transform.localPosition = GetCenter(Selection.transforms);
		grp.transform.localRotation = Quaternion.identity;
		grp.transform.localScale = Vector3.one;
		foreach (Transform t in Selection.transforms) {
			t.parent = grp.transform;
		}
	}
	
	static Vector3 GetCenter (Transform[] tArray) {
		Vector3 center = Vector3.zero;
		foreach (Transform t in tArray) {
			if (t) center += t.position;
		}
		center /= (float)tArray.Length;
		return center;
	}
	
	static bool ValidateGroupObjectsCenter() {
		return Selection.transforms.Length > 0;
	}
	
	[MenuItem("Tools/Center on Children #&g")]
	static void CenterObjectsOnChildren () {
		Undo.RegisterSceneUndo("Center");
		foreach (Transform t in Selection.transforms) {
			if (t.childCount > 0) CenterObjectOnChildren(t);	
		}
	}

	static void CenterObjectOnChildren (Transform t) {
		Transform[] tArray = t.GetComponentsInChildren<Transform>() as Transform[];
		Transform[] children = new Transform[tArray.Length];
		
		int count = 0;
		foreach(Transform trans in tArray) {
			if (trans != t) {
				trans.parent = t.parent;
				children[count] = trans;
				count++;
			}
		}
			
		t.position = GetCenter(children);
		foreach(Transform child in children) {
			if (child) child.parent = t;	
		}
	}
	
	[MenuItem("Tools/Center on Origin &g")]
	static void CenterObjectsOnOrigin () {
		Undo.RegisterSceneUndo("Center");
		foreach (Transform t in Selection.transforms) {
			if (t.childCount > 0) CenterObjectOnOrigin(t);	
		}
	}
	
	static void CenterObjectOnOrigin (Transform t) {
		Transform[] tArray = t.GetComponentsInChildren<Transform>() as Transform[];
		Transform[] children = new Transform[tArray.Length];
		
		int count = 0;
		foreach(Transform trans in tArray) {
			if (trans != t) {
				trans.parent = t.parent;
				children[count] = trans;
				count++;
			}
		}
			
		t.position = Vector3.zero;
		foreach(Transform child in children) {
			if (child) child.parent = t;	
		}
	}

	[MenuItem("Tools/Add Child &%c", true)]
	[MenuItem("Tools/Group at Origin %g", true)]
	[MenuItem("Tools/Group at Center #%g", true)]
	[MenuItem("Tools/Center on Origin &g", true)]
	[MenuItem("Tools/Center on Children #&g", true)]
	static bool ValidateGroup() {
		return Selection.transforms.Length > 0;
	}
}
