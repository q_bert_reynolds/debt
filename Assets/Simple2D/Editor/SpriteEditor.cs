using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(Sprite))]
[CanEditMultipleObjects]
public class SpriteEditor : Editor {
	
	static bool isEditing = false;

	[MenuItem("GameObject/Create Other/Sprite")]
    public static void CreateAsset () {
		GameObject spr = new GameObject("Sprite");
		spr.AddComponent<Sprite>();
        Selection.activeObject = spr;
	}
	
	public override void OnInspectorGUI () {	
		Sprite s = (Sprite)target;
		
		s.sheet = (SpriteSheet)EditorGUILayout.ObjectField("Sheet", s.sheet, typeof(SpriteSheet), true);
		Color color = EditorGUILayout.ColorField("Color", s.color);
		Vector3 anchor = EditorGUILayout.Vector3Field("Anchor Point", s.anchor);
		
		if (s.sheet != null) {
			string[] spriteNames = s.sheet.spriteNames;
			int currentIndex = 0;
			for (int i = 0; i < spriteNames.Length; i++) {
				if (spriteNames[i] == s.spriteName) {
					currentIndex = i;
					break;
				}
			}
			
			EditorGUILayout.BeginHorizontal();
			int[] indices = new int[3];
			indices[0] = GUILayout.Button("<")?currentIndex - 1:currentIndex;
			indices[1] = EditorGUILayout.Popup(currentIndex, spriteNames);
			indices[2] = GUILayout.Button(">")?currentIndex + 1:currentIndex;
			int index = currentIndex;
			foreach (int i in indices) {
				if (i != index) {
					index = i % spriteNames.Length;
					if (index < 0) index += spriteNames.Length;
					break;
				}
			}
			EditorGUILayout.EndHorizontal();
			
			s.spriteName = spriteNames[index];
			SpriteInfo info = s.sheet.spriteInfoList[index];
			
			Rect lastGUIRect = GUILayoutUtility.GetLastRect();
			float w = lastGUIRect.width;
			float t = lastGUIRect.yMax;
			
			GUI.color = color;
			Rect textureRect = new Rect((w - info.frame.x) * 0.5f, t, info.frame.x, info.frame.y);
			if (info.rotated) {
				Rect rotatedRect = new Rect((w - info.frame.y) * 0.5f, t + (info.frame.y - info.frame.x) * 0.5f, info.frame.y, info.frame.x);
				Matrix4x4 temp = GUI.matrix;
		        GUIUtility.RotateAroundPivot(-90, rotatedRect.center);
				GUI.DrawTextureWithTexCoords(rotatedRect, s.sheet.sheetTexture, info.uvs);
		        GUI.matrix = temp;
			}
			else {
				GUI.DrawTextureWithTexCoords(textureRect, s.sheet.sheetTexture, info.uvs);
			}
			GUILayout.Space(info.frame.y);
			GUI.color = Color.white;
			
			Vector3 a = s.anchor;
			a.x *= -1;
			Vector3 center = (Vector3)textureRect.center + a;
			Handles.DrawWireDisc(center, Vector3.forward, 4);
			
			if (!isEditing && GUILayout.Button("Edit Anchor Point")) {
				isEditing = true;
			}
			else if (isEditing && GUILayout.Button("Stop Editing")) {
				isEditing = false;
			}

			Event e = Event.current;
			if (isEditing && e.isMouse && ((e.type == EventType.MouseDown && textureRect.Contains(e.mousePosition)) || e.type == EventType.MouseDrag)) {
				anchor = (Vector3)(e.mousePosition - textureRect.center);
				anchor.x *= -1;
			}
			
			if (GUILayout.Button("Bake Scale")) {
				s.BakeScale();
				EditorUtility.SetDirty(s);
			}
				
			if (color != s.color || anchor != s.anchor || currentIndex != index || GUILayout.Button("Refresh Sprite")) {
				Animation ani = GetRootAnimation(s);
				
				if (AnimationUtility.InAnimationMode() && ani != null) {
					AnimationClip clip = ani.clip;
					string path = AnimationUtility.CalculateTransformPath(s.transform, ani.transform);
					AnimationCurve curve = AnimationUtility.GetEditorCurve(clip, path, typeof(Sprite), "spriteID");
					if (curve != null) {
						Keyframe[] keys = new Keyframe[curve.keys.Length];
						for (int i = 0; i < keys.Length; i++) {
							keys[i] = AnimUtils.SetKeyTangentMode(curve.keys[i], AnimUtils.TangentMode.Stepped, AnimUtils.TangentMode.Stepped);
						}
						for (int i = 0; i < keys.Length; i++) {
							keys[i] = AnimUtils.CleanKeyTangentSlopes(keys, i);	
						}
						AnimationCurve newCurve = new AnimationCurve(keys);
						AnimationUtility.SetEditorCurve(clip, path, typeof(Sprite), "spriteID", newCurve);
						AssetDatabase.SaveAssets();
						AssetDatabase.Refresh();
					}
				}
				
				s.color = color;
				s.anchor = anchor;
				s.UpdateMesh();
				EditorUtility.SetDirty(s);
			}	
		}
	}	
	
	Animation GetRootAnimation (Sprite s, int maxDepth = 10) {
		Transform t = s.transform;
		for (int i = 0; i < maxDepth; i++) {
			if (t.animation != null)
				return t.animation;
			t = t.parent;
			if (t == null) break;
		}
		return null;
	}
}
