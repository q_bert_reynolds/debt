Shader "Simple2D/Screen" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
			
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Blend One OneMinusSrcColor
		Cull Off Lighting Off ZWrite Off
		
		BindChannels {
			Bind "Color", color
			Bind "Vertex", vertex
			Bind "TexCoord", texcoord
		}
		Pass {
			SetTexture [_MainTex] {
				combine texture * primary
			}
			SetTexture [_MainTex] {
				constantColor (0,0,0,0)
				combine previous lerp (previous) constant
			}
		}
	}
	FallBack "Mobile/Particles/Alpha Blended"
}
