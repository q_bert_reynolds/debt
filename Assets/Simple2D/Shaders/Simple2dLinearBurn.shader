Shader "Simple2D/Linear Burn" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
			
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Blend One One
		BlendOp RevSub
		Cull Off Lighting Off ZWrite Off
		
		BindChannels {
			Bind "Color", color
			Bind "Vertex", vertex
			Bind "TexCoord", texcoord
		}
		Pass {
			SetTexture [_MainTex] {
				combine texture * primary
			}
			SetTexture [_MainTex] {
				constantColor (1,1,1,1)
				combine constant - previous
			}
			SetTexture [_MainTex] {
				constantColor (0,0,0,0)
				combine previous lerp (texture) constant
			}
		}
	}
	FallBack "Mobile/Particles/Alpha Blended"
}
