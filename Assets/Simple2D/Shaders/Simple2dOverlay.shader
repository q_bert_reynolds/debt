Shader "Simple2D/Overlay" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
			
	SubShader {
		Tags {
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
		} 
		
		Cull Off Lighting Off ZWrite Off
		 		
		// Multiply Pass
		Pass {
			Blend DstColor Zero
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			
			struct v2f {
    			float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
			};

			float4 _MainTex_ST;
			
			v2f vert (appdata_full v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.color = v.color;
				return o;
			}

			half4 frag (v2f i) : COLOR {
				half4 c = tex2D(_MainTex, i.uv) * i.color;
				float colorValue = max(max(c.r, c.g), c.b);
				float shouldMult = floor(clamp(colorValue + 0.5, 0 ,1));
				return lerp(c, 1, 1 - c.a*(1-shouldMult));
			}
			ENDCG
		}
		
		// Screen Pass
		Pass {
			Blend OneMinusDstColor One
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			
			struct v2f {
    			float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
			};

			float4 _MainTex_ST;
			
			v2f vert (appdata_full v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.color = v.color;
				return o;
			}

			half4 frag (v2f i) : COLOR {
				half4 c = tex2D(_MainTex, i.uv) * i.color;
				float colorValue = max(max(c.r, c.g), c.b);
				float shouldScreen = ceil(clamp(colorValue - 0.5, 0 ,1));
				return c * c.a * shouldScreen;
			}
			ENDCG
		}
	}
	FallBack "Mobile/Particles/Alpha Blended"
}
